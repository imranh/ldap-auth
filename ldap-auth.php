<?php

/*
Written by Imran Hussain ~imranh

Used to auth people, will check SUCS then the uni ldap, will only check
students on the uni ldap.

will return "sucs" if the username/password passed is a sucs member
will return "uni" if the user/pass passed has a student swan uni account
will return "nope" if the user/pass passed is inavlid

Example usage:

require "ldap-auth.php";

isAuthd = ldapAuth("usaername", "password");

if (isAuthd == "sucs"){
	//do stuff for sucs auth
}elseif (isAuthd == "uni"){
	//do stuff for uni auth
}else{
	//do stuff for not authd peeps
}

*/

// we don't care about warnings, we write our own
error_reporting(E_ERROR | E_PARSE);

define(LDAP_OPT_DIAGNOSTIC_MESSAGE, 0x0032);

function ldapAuth($username, $password)
{

    if ($username != "" && $password != "") {

        // people like to use emails to login so lets detect and strip
        if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            //valid email, lets strip
            // split the email into a string array "@" as a delim
            $s = explode("@", $username);
            // remove the last element (domain)
            array_pop($s);
            // put the array back togther using "@" as a seperator
            $username = implode("@", $s);
        }

        // filter out everything but A-Z a-z 0-9 . - _ from username
        $safeusername = preg_replace("/[^A-Za-z0-9\.\-\_]/", '', $username);

        // if safeusername isn't the same as username just error out
        if ($safeusername != $username) {
            return "nope";
        }

        // ldap servers
        $sucsLDAPServer = 'silver.sucs.swan.ac.uk';
        $issLDAPServer = '192.168.10.16';

        // how to bind
        $sucsBindDn = "uid=$safeusername,ou=People,dc=sucs,dc=org";
        $issBindDn = "cn=$safeusername,ou=Students,ou=Active,ou=Resources,o=Swansea";

        // Main auth

        // Try and connect to silver
        $ldapconnSUCS = ldap_connect($sucsLDAPServer) or die("Could not connect to SUCS LDAP server.");

        ldap_set_option($ldapconnSUCS,LDAP_OPT_PROTOCOL_VERSION,3);

        if ($ldapconnSUCS) {

            //echo "Connected to $sucsLDAPServer <br>";

            // try and bind to sucs ldap
            $ldapbindSUCS = ldap_bind($ldapconnSUCS, $sucsBindDn, $password);

            if ($ldapbindSUCS) {
                //echo "Auth'd as $username using SUCS LDAP<br>";
                return "sucs";
                // turns out they didn't give us valid sucs creds, lets try iss now
            } else {

                // try and connect to the iss ldap server
                $ldapconnISS = ldap_connect($issLDAPServer) or die("Could not connect to uni LDAP server.");
                // echo "Connected to $issLDAPServer <br>";

                ldap_set_option($ldapconnISS,LDAP_OPT_PROTOCOL_VERSION,3);

                // lets try and bind to the uni ldap
                $ldapbindiss = ldap_bind($ldapconnISS, $issBindDn, $password);

                /*if (ldap_get_option($ldapconnISS, LDAP_OPT_DIAGNOSTIC_MESSAGE, $extended_error)) {
                   echo "Error Binding to LDAP: $extended_error";
                }*/

                if ($ldapbindiss) {
                    //echo "Auth'd as $username using uni LDAP using ou=$issUsernameOu<br>";
                    return "uni";
                } else {
                    //exit("Invalid Username or Password");
                    return "nope";
                }
            }
        }
    } else {
        return "nope";
    }
}

?>